﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;

namespace B181210350.Models
{
    public class Ordek : Hayvan
    {
        public Ordek()
        {
            HayvanAdı = "Ördek";
            HayvanEnerji = 100;
            MahsulSuresi = 5;
            OlumDeger = -3;
            YasamDurumu = true;
            UrunSayisi = 0;
        }
        public override void HayvanSesCikar()
        {
            SoundPlayer player = new SoundPlayer(Properties.Resources.ordekSound);
            player.Play();
        }

        public override int Hesapla()
        {
            return UrunSayisi * 3;
        }
    }
}

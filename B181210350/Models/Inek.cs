﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;

namespace B181210350.Models
{
    public class Inek : Hayvan
    {
        public Inek()
        {
            HayvanAdı = "İnek";
            HayvanEnerji = 100;
            MahsulSuresi = 8;
            OlumDeger = -8;
            YasamDurumu = true;
            UrunSayisi = 0;
        }
        public override void HayvanSesCikar()
        {
            SoundPlayer player = new SoundPlayer(Properties.Resources.inekSound);
            player.Play();
        }

        public override int Hesapla()
        {
            return UrunSayisi * 5;
        }
    }
}

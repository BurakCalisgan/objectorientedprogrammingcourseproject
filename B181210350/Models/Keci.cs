﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;

namespace B181210350.Models
{
    public class Keci : Hayvan
    {
        public Keci()
        {
            HayvanAdı = "Keçi";
            HayvanEnerji = 100;
            MahsulSuresi = 7;
            OlumDeger = -6;
            YasamDurumu = true;
            UrunSayisi = 0;
        }
        public override void HayvanSesCikar()
        {
            SoundPlayer player = new SoundPlayer(Properties.Resources.keciSound);
            player.Play();
        }

        public override int Hesapla()
        {
            return UrunSayisi * 8;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Media;

namespace B181210350.Models
{
    public abstract class Hayvan
    {
        public string HayvanAdı { get; set; }
        public int HayvanEnerji { get; set; }
        public int MahsulSuresi { get; set; }
        public int OlumDeger { get; set; }
        public bool YasamDurumu { get; set; }
        public int UrunSayisi { get; set; }
        public abstract void HayvanSesCikar();
        public abstract int Hesapla();
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;

namespace B181210350.Models
{
    public class Tavuk : Hayvan
    {
        public Tavuk()
        {
            HayvanAdı = "Tavuk";
            HayvanEnerji = 100;
            MahsulSuresi = 3;
            OlumDeger = -2;
            YasamDurumu = true;
            UrunSayisi = 0;
        }
        public override void HayvanSesCikar()
        {
            SoundPlayer player = new SoundPlayer(Properties.Resources.tavukSound);
            player.Play();
        }

        public override int Hesapla()
        {
            return UrunSayisi * 1;
        }
    }
}

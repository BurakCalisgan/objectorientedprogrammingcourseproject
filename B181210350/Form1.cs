﻿using B181210350.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace B181210350
{
    public partial class Form1 : Form
    {
        Tavuk tavuk;
        Inek inek;
        Ordek ordek;
        Keci keci;

        int zaman = 0;
        int toplamKazanc = 0;
        public Form1()
        {
            InitializeComponent();

            tavuk = new Tavuk();
            inek = new Inek();
            ordek = new Ordek();
            keci = new Keci();

            this.pbTavuk.Increment(tavuk.HayvanEnerji);
            this.pbInek.Increment(inek.HayvanEnerji);
            this.pbOrdek.Increment(ordek.HayvanEnerji);
            this.pbKeci.Increment(keci.HayvanEnerji);

        }

        private void timer_Tick(object sender, EventArgs e)
        {
            zaman++;
            lblSn.Text = Convert.ToString(zaman) + " SN";


            if (tavuk.YasamDurumu)
            {
                tavuk.HayvanEnerji = tavuk.HayvanEnerji + tavuk.OlumDeger; //Ölüm değer - işaretli
                this.pbTavuk.Increment(tavuk.OlumDeger);
                if (tavuk.HayvanEnerji <= 0)
                {
                    tavuk.YasamDurumu = false;
                    tavuk.HayvanSesCikar();
                }

                if (zaman % tavuk.MahsulSuresi == 0)
                {
                    tavuk.UrunSayisi++;
                    lblTavukAdet.Text = tavuk.UrunSayisi.ToString() + " ADET";
                }
            }



            if (inek.YasamDurumu)
            {
                inek.HayvanEnerji = inek.HayvanEnerji + inek.OlumDeger;
                this.pbInek.Increment(inek.OlumDeger);
                if (inek.HayvanEnerji <= 0)
                {
                    inek.YasamDurumu = false;
                    inek.HayvanSesCikar();
                }
                if (zaman % inek.MahsulSuresi == 0)
                {
                    inek.UrunSayisi++;
                    lblInekAdet.Text = inek.UrunSayisi.ToString() + " KG";
                }
            }



            if (ordek.YasamDurumu)
            {
                ordek.HayvanEnerji = ordek.HayvanEnerji + ordek.OlumDeger;
                this.pbOrdek.Increment(ordek.OlumDeger);
                if (ordek.HayvanEnerji <= 0)
                {
                    ordek.YasamDurumu = false;
                    ordek.HayvanSesCikar();
                }
                if (zaman % ordek.MahsulSuresi == 0)
                {
                    ordek.UrunSayisi++;
                    lblOrdekAdet.Text = ordek.UrunSayisi.ToString() + " ADET";
                }
            }



            if (keci.YasamDurumu)
            {
                keci.HayvanEnerji = keci.HayvanEnerji + keci.OlumDeger;
                this.pbKeci.Increment(keci.OlumDeger);
                if (keci.HayvanEnerji <= 0)
                {
                    keci.YasamDurumu = false;
                    keci.HayvanSesCikar();
                }

                if (zaman % keci.MahsulSuresi == 0)
                {
                    keci.UrunSayisi++;
                    lblKeciAdet.Text = keci.UrunSayisi.ToString() + " KG";
                }
            }


        }

        private void btnTavukYemVer_Click(object sender, EventArgs e)
        {
            if (tavuk.YasamDurumu)
            {
                tavuk.HayvanEnerji = 100;
                this.pbTavuk.Increment(tavuk.HayvanEnerji);
            }

        }

        private void btnInekYemVer_Click(object sender, EventArgs e)
        {

            if (inek.YasamDurumu)
            {
                inek.HayvanEnerji = 100;
                this.pbInek.Increment(inek.HayvanEnerji);
            }
        }

        private void btnOrdekYemVer_Click(object sender, EventArgs e)
        {
            if (ordek.YasamDurumu)
            {
                ordek.HayvanEnerji = 100;
                this.pbOrdek.Increment(ordek.HayvanEnerji);
            }

        }

        private void btnKeciYemVer_Click(object sender, EventArgs e)
        {
            if (keci.YasamDurumu)
            {
                keci.HayvanEnerji = 100;
                this.pbKeci.Increment(keci.HayvanEnerji);
            }

        }


        private void btnTavukHesapla_Click(object sender, EventArgs e)
        {
            if (tavuk.UrunSayisi > 0)
            {
                toplamKazanc += tavuk.Hesapla();
                lblKasa.Text = toplamKazanc.ToString() + " TL";

                tavuk.UrunSayisi = 0;
                lblTavukAdet.Text = tavuk.UrunSayisi.ToString() + " ADET";
            }

        }

        private void btnOrdekHesapla_Click(object sender, EventArgs e)
        {
            if (ordek.UrunSayisi > 0)
            {
                toplamKazanc += ordek.Hesapla();
                lblKasa.Text = toplamKazanc.ToString() + " TL";

                ordek.UrunSayisi = 0;
                lblOrdekAdet.Text = ordek.UrunSayisi.ToString() + " ADET";
            }
        }

        private void btnInekHesapla_Click(object sender, EventArgs e)
        {
            if (inek.UrunSayisi > 0)
            {
                toplamKazanc += inek.Hesapla();
                lblKasa.Text = toplamKazanc.ToString() + " TL";

                inek.UrunSayisi = 0;
                lblInekAdet.Text = inek.UrunSayisi.ToString() + " ADET";
            }

        }

        private void btnKeciHesapla_Click(object sender, EventArgs e)
        {
            if (keci.UrunSayisi > 0)
            {
                toplamKazanc += keci.Hesapla();
                lblKasa.Text = toplamKazanc.ToString() + " TL";

                keci.UrunSayisi = 0;
                lblKeciAdet.Text = keci.UrunSayisi.ToString() + " ADET";
            }
        }
    }
}

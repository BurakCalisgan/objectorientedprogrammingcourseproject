﻿namespace B181210350
{
    partial class Form1
    {
        /// <summary>
        ///Gerekli tasarımcı değişkeni.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///Kullanılan tüm kaynakları temizleyin.
        /// </summary>
        ///<param name="disposing">yönetilen kaynaklar dispose edilmeliyse doğru; aksi halde yanlış.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer üretilen kod

        /// <summary>
        /// Tasarımcı desteği için gerekli metot - bu metodun 
        ///içeriğini kod düzenleyici ile değiştirmeyin.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lblKasa = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblSn = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnKeciHesapla = new System.Windows.Forms.Button();
            this.btnInekHesapla = new System.Windows.Forms.Button();
            this.btnOrdekHesapla = new System.Windows.Forms.Button();
            this.btnTavukHesapla = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblKeciAdet = new System.Windows.Forms.Label();
            this.lblOrdekAdet = new System.Windows.Forms.Label();
            this.lblInekAdet = new System.Windows.Forms.Label();
            this.lblTavukAdet = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnKeciYemVer = new System.Windows.Forms.Button();
            this.btnInekYemVer = new System.Windows.Forms.Button();
            this.pbKeci = new System.Windows.Forms.ProgressBar();
            this.label20 = new System.Windows.Forms.Label();
            this.pbInek = new System.Windows.Forms.ProgressBar();
            this.label22 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label32 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnOrdekYemVer = new System.Windows.Forms.Button();
            this.btnTavukYemVer = new System.Windows.Forms.Button();
            this.pbOrdek = new System.Windows.Forms.ProgressBar();
            this.label18 = new System.Windows.Forms.Label();
            this.pbTavuk = new System.Windows.Forms.ProgressBar();
            this.label15 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label23 = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(2);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.Teal;
            this.splitContainer1.Panel1.Controls.Add(this.panel4);
            this.splitContainer1.Panel1.Controls.Add(this.panel3);
            this.splitContainer1.Panel1.Controls.Add(this.panel2);
            this.splitContainer1.Panel1.Controls.Add(this.panel1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.Moccasin;
            this.splitContainer1.Panel2.Controls.Add(this.panel6);
            this.splitContainer1.Panel2.Controls.Add(this.panel5);
            this.splitContainer1.Size = new System.Drawing.Size(600, 366);
            this.splitContainer1.SplitterDistance = 178;
            this.splitContainer1.SplitterWidth = 3;
            this.splitContainer1.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel4.Controls.Add(this.lblKasa);
            this.panel4.Controls.Add(this.label14);
            this.panel4.Location = new System.Drawing.Point(496, 91);
            this.panel4.Margin = new System.Windows.Forms.Padding(2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(102, 85);
            this.panel4.TabIndex = 3;
            // 
            // lblKasa
            // 
            this.lblKasa.BackColor = System.Drawing.Color.LightBlue;
            this.lblKasa.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblKasa.ForeColor = System.Drawing.Color.MidnightBlue;
            this.lblKasa.Location = new System.Drawing.Point(2, 29);
            this.lblKasa.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblKasa.Name = "lblKasa";
            this.lblKasa.Size = new System.Drawing.Size(98, 50);
            this.lblKasa.TabIndex = 9;
            this.lblKasa.Text = "0 TL";
            this.lblKasa.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.Color.Orange;
            this.label14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label14.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label14.Location = new System.Drawing.Point(2, 5);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(98, 19);
            this.label14.TabIndex = 8;
            this.label14.Text = "KASA";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel3.Controls.Add(this.lblSn);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Location = new System.Drawing.Point(496, 2);
            this.panel3.Margin = new System.Windows.Forms.Padding(2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(102, 85);
            this.panel3.TabIndex = 2;
            // 
            // lblSn
            // 
            this.lblSn.BackColor = System.Drawing.Color.LightBlue;
            this.lblSn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblSn.ForeColor = System.Drawing.Color.MidnightBlue;
            this.lblSn.Location = new System.Drawing.Point(2, 29);
            this.lblSn.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSn.Name = "lblSn";
            this.lblSn.Size = new System.Drawing.Size(98, 50);
            this.lblSn.TabIndex = 7;
            this.lblSn.Tag = "";
            this.lblSn.Text = "0 SN";
            this.lblSn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.Orange;
            this.label11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label11.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label11.Location = new System.Drawing.Point(2, 5);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(98, 19);
            this.label11.TabIndex = 2;
            this.label11.Text = "GEÇEN SÜRE";
            this.label11.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel2.Controls.Add(this.btnKeciHesapla);
            this.panel2.Controls.Add(this.btnInekHesapla);
            this.panel2.Controls.Add(this.btnOrdekHesapla);
            this.panel2.Controls.Add(this.btnTavukHesapla);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Location = new System.Drawing.Point(284, 2);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(207, 174);
            this.panel2.TabIndex = 1;
            // 
            // btnKeciHesapla
            // 
            this.btnKeciHesapla.BackColor = System.Drawing.Color.DarkSalmon;
            this.btnKeciHesapla.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnKeciHesapla.ForeColor = System.Drawing.Color.DarkRed;
            this.btnKeciHesapla.Location = new System.Drawing.Point(10, 139);
            this.btnKeciHesapla.Margin = new System.Windows.Forms.Padding(2);
            this.btnKeciHesapla.Name = "btnKeciHesapla";
            this.btnKeciHesapla.Size = new System.Drawing.Size(188, 26);
            this.btnKeciHesapla.TabIndex = 5;
            this.btnKeciHesapla.Text = "KEÇİ SÜTÜ SAT";
            this.btnKeciHesapla.UseVisualStyleBackColor = false;
            this.btnKeciHesapla.Click += new System.EventHandler(this.btnKeciHesapla_Click);
            // 
            // btnInekHesapla
            // 
            this.btnInekHesapla.BackColor = System.Drawing.Color.DarkSalmon;
            this.btnInekHesapla.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnInekHesapla.ForeColor = System.Drawing.Color.DarkRed;
            this.btnInekHesapla.Location = new System.Drawing.Point(10, 105);
            this.btnInekHesapla.Margin = new System.Windows.Forms.Padding(2);
            this.btnInekHesapla.Name = "btnInekHesapla";
            this.btnInekHesapla.Size = new System.Drawing.Size(188, 26);
            this.btnInekHesapla.TabIndex = 4;
            this.btnInekHesapla.Text = "İNEK SÜTÜ SAT";
            this.btnInekHesapla.UseVisualStyleBackColor = false;
            this.btnInekHesapla.Click += new System.EventHandler(this.btnInekHesapla_Click);
            // 
            // btnOrdekHesapla
            // 
            this.btnOrdekHesapla.BackColor = System.Drawing.Color.DarkSalmon;
            this.btnOrdekHesapla.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnOrdekHesapla.ForeColor = System.Drawing.Color.DarkRed;
            this.btnOrdekHesapla.Location = new System.Drawing.Point(10, 72);
            this.btnOrdekHesapla.Margin = new System.Windows.Forms.Padding(2);
            this.btnOrdekHesapla.Name = "btnOrdekHesapla";
            this.btnOrdekHesapla.Size = new System.Drawing.Size(188, 26);
            this.btnOrdekHesapla.TabIndex = 3;
            this.btnOrdekHesapla.Text = "ÖRDEK YUMURTASI SAT";
            this.btnOrdekHesapla.UseVisualStyleBackColor = false;
            this.btnOrdekHesapla.Click += new System.EventHandler(this.btnOrdekHesapla_Click);
            // 
            // btnTavukHesapla
            // 
            this.btnTavukHesapla.BackColor = System.Drawing.Color.DarkSalmon;
            this.btnTavukHesapla.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnTavukHesapla.ForeColor = System.Drawing.Color.DarkRed;
            this.btnTavukHesapla.Location = new System.Drawing.Point(10, 40);
            this.btnTavukHesapla.Margin = new System.Windows.Forms.Padding(2);
            this.btnTavukHesapla.Name = "btnTavukHesapla";
            this.btnTavukHesapla.Size = new System.Drawing.Size(188, 26);
            this.btnTavukHesapla.TabIndex = 2;
            this.btnTavukHesapla.Text = "TAVUK YUMURTASI SAT";
            this.btnTavukHesapla.UseVisualStyleBackColor = false;
            this.btnTavukHesapla.Click += new System.EventHandler(this.btnTavukHesapla_Click);
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.Orange;
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label10.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label10.Location = new System.Drawing.Point(10, 7);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(188, 26);
            this.label10.TabIndex = 1;
            this.label10.Text = "GIDA FABRİKASI";
            this.label10.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel1.Controls.Add(this.lblKeciAdet);
            this.panel1.Controls.Add(this.lblOrdekAdet);
            this.panel1.Controls.Add(this.lblInekAdet);
            this.panel1.Controls.Add(this.lblTavukAdet);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(9, 2);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(271, 174);
            this.panel1.TabIndex = 0;
            // 
            // lblKeciAdet
            // 
            this.lblKeciAdet.BackColor = System.Drawing.Color.LightBlue;
            this.lblKeciAdet.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblKeciAdet.ForeColor = System.Drawing.Color.MidnightBlue;
            this.lblKeciAdet.Location = new System.Drawing.Point(145, 139);
            this.lblKeciAdet.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblKeciAdet.Name = "lblKeciAdet";
            this.lblKeciAdet.Size = new System.Drawing.Size(110, 26);
            this.lblKeciAdet.TabIndex = 8;
            this.lblKeciAdet.Text = "0 ADET";
            this.lblKeciAdet.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblOrdekAdet
            // 
            this.lblOrdekAdet.BackColor = System.Drawing.Color.LightBlue;
            this.lblOrdekAdet.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblOrdekAdet.ForeColor = System.Drawing.Color.MidnightBlue;
            this.lblOrdekAdet.Location = new System.Drawing.Point(8, 139);
            this.lblOrdekAdet.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblOrdekAdet.Name = "lblOrdekAdet";
            this.lblOrdekAdet.Size = new System.Drawing.Size(117, 26);
            this.lblOrdekAdet.TabIndex = 7;
            this.lblOrdekAdet.Text = "0 ADET";
            this.lblOrdekAdet.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblInekAdet
            // 
            this.lblInekAdet.BackColor = System.Drawing.Color.LightBlue;
            this.lblInekAdet.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblInekAdet.ForeColor = System.Drawing.Color.MidnightBlue;
            this.lblInekAdet.Location = new System.Drawing.Point(145, 69);
            this.lblInekAdet.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblInekAdet.Name = "lblInekAdet";
            this.lblInekAdet.Size = new System.Drawing.Size(110, 26);
            this.lblInekAdet.TabIndex = 6;
            this.lblInekAdet.Text = "0 KG";
            this.lblInekAdet.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTavukAdet
            // 
            this.lblTavukAdet.BackColor = System.Drawing.Color.LightBlue;
            this.lblTavukAdet.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblTavukAdet.ForeColor = System.Drawing.Color.MidnightBlue;
            this.lblTavukAdet.Location = new System.Drawing.Point(8, 69);
            this.lblTavukAdet.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTavukAdet.Name = "lblTavukAdet";
            this.lblTavukAdet.Size = new System.Drawing.Size(117, 26);
            this.lblTavukAdet.TabIndex = 5;
            this.lblTavukAdet.Text = "0 ADET";
            this.lblTavukAdet.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Bisque;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label4.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label4.Location = new System.Drawing.Point(145, 108);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(110, 20);
            this.label4.TabIndex = 4;
            this.label4.Text = "KEÇİ SÜTÜ";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Bisque;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label5.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label5.Location = new System.Drawing.Point(8, 108);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(117, 20);
            this.label5.TabIndex = 3;
            this.label5.Text = "ÖRDEK YUMURTASI";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Bisque;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label3.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label3.Location = new System.Drawing.Point(145, 42);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "İNEK SÜTÜ";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Bisque;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label2.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label2.Location = new System.Drawing.Point(8, 42);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "TAVUK YUMURTASI";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Orange;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(10, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(249, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "ÜRÜN DEPOSU";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel6.Controls.Add(this.btnKeciYemVer);
            this.panel6.Controls.Add(this.btnInekYemVer);
            this.panel6.Controls.Add(this.pbKeci);
            this.panel6.Controls.Add(this.label20);
            this.panel6.Controls.Add(this.pbInek);
            this.panel6.Controls.Add(this.label22);
            this.panel6.Controls.Add(this.pictureBox3);
            this.panel6.Controls.Add(this.pictureBox4);
            this.panel6.Controls.Add(this.label32);
            this.panel6.Location = new System.Drawing.Point(303, 2);
            this.panel6.Margin = new System.Windows.Forms.Padding(2);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(295, 179);
            this.panel6.TabIndex = 2;
            // 
            // btnKeciYemVer
            // 
            this.btnKeciYemVer.BackColor = System.Drawing.Color.Bisque;
            this.btnKeciYemVer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnKeciYemVer.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnKeciYemVer.Location = new System.Drawing.Point(114, 147);
            this.btnKeciYemVer.Margin = new System.Windows.Forms.Padding(2);
            this.btnKeciYemVer.Name = "btnKeciYemVer";
            this.btnKeciYemVer.Size = new System.Drawing.Size(174, 22);
            this.btnKeciYemVer.TabIndex = 18;
            this.btnKeciYemVer.Text = "YEM VER";
            this.btnKeciYemVer.UseVisualStyleBackColor = false;
            this.btnKeciYemVer.Click += new System.EventHandler(this.btnKeciYemVer_Click);
            // 
            // btnInekYemVer
            // 
            this.btnInekYemVer.BackColor = System.Drawing.Color.Bisque;
            this.btnInekYemVer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnInekYemVer.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnInekYemVer.Location = new System.Drawing.Point(114, 76);
            this.btnInekYemVer.Margin = new System.Windows.Forms.Padding(2);
            this.btnInekYemVer.Name = "btnInekYemVer";
            this.btnInekYemVer.Size = new System.Drawing.Size(174, 22);
            this.btnInekYemVer.TabIndex = 17;
            this.btnInekYemVer.Text = "YEM VER";
            this.btnInekYemVer.UseVisualStyleBackColor = false;
            this.btnInekYemVer.Click += new System.EventHandler(this.btnInekYemVer_Click);
            // 
            // pbKeci
            // 
            this.pbKeci.Location = new System.Drawing.Point(114, 125);
            this.pbKeci.Margin = new System.Windows.Forms.Padding(2);
            this.pbKeci.Name = "pbKeci";
            this.pbKeci.Size = new System.Drawing.Size(174, 20);
            this.pbKeci.TabIndex = 15;
            // 
            // label20
            // 
            this.label20.BackColor = System.Drawing.Color.Bisque;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label20.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label20.Location = new System.Drawing.Point(114, 103);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(174, 20);
            this.label20.TabIndex = 14;
            this.label20.Text = "CANLI";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pbInek
            // 
            this.pbInek.Location = new System.Drawing.Point(114, 54);
            this.pbInek.Margin = new System.Windows.Forms.Padding(2);
            this.pbInek.Name = "pbInek";
            this.pbInek.Size = new System.Drawing.Size(174, 20);
            this.pbInek.TabIndex = 12;
            // 
            // label22
            // 
            this.label22.BackColor = System.Drawing.Color.Bisque;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label22.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label22.Location = new System.Drawing.Point(114, 32);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(174, 20);
            this.label22.TabIndex = 11;
            this.label22.Text = "CANLI";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::B181210350.Properties.Resources.keci;
            this.pictureBox3.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox3.InitialImage")));
            this.pictureBox3.Location = new System.Drawing.Point(2, 103);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(107, 68);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 10;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::B181210350.Properties.Resources.inek;
            this.pictureBox4.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox4.InitialImage")));
            this.pictureBox4.Location = new System.Drawing.Point(2, 32);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(107, 66);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 9;
            this.pictureBox4.TabStop = false;
            // 
            // label32
            // 
            this.label32.BackColor = System.Drawing.Color.Orange;
            this.label32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label32.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label32.Location = new System.Drawing.Point(2, 7);
            this.label32.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(291, 21);
            this.label32.TabIndex = 0;
            this.label32.Text = "AHIR";
            this.label32.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel5.Controls.Add(this.btnOrdekYemVer);
            this.panel5.Controls.Add(this.btnTavukYemVer);
            this.panel5.Controls.Add(this.pbOrdek);
            this.panel5.Controls.Add(this.label18);
            this.panel5.Controls.Add(this.pbTavuk);
            this.panel5.Controls.Add(this.label15);
            this.panel5.Controls.Add(this.pictureBox2);
            this.panel5.Controls.Add(this.pictureBox1);
            this.panel5.Controls.Add(this.label23);
            this.panel5.Location = new System.Drawing.Point(9, 2);
            this.panel5.Margin = new System.Windows.Forms.Padding(2);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(290, 179);
            this.panel5.TabIndex = 1;
            // 
            // btnOrdekYemVer
            // 
            this.btnOrdekYemVer.BackColor = System.Drawing.Color.Bisque;
            this.btnOrdekYemVer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnOrdekYemVer.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnOrdekYemVer.Location = new System.Drawing.Point(113, 147);
            this.btnOrdekYemVer.Margin = new System.Windows.Forms.Padding(2);
            this.btnOrdekYemVer.Name = "btnOrdekYemVer";
            this.btnOrdekYemVer.Size = new System.Drawing.Size(174, 22);
            this.btnOrdekYemVer.TabIndex = 10;
            this.btnOrdekYemVer.Text = "YEM VER";
            this.btnOrdekYemVer.UseVisualStyleBackColor = false;
            this.btnOrdekYemVer.Click += new System.EventHandler(this.btnOrdekYemVer_Click);
            // 
            // btnTavukYemVer
            // 
            this.btnTavukYemVer.BackColor = System.Drawing.Color.Bisque;
            this.btnTavukYemVer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnTavukYemVer.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnTavukYemVer.Location = new System.Drawing.Point(113, 76);
            this.btnTavukYemVer.Margin = new System.Windows.Forms.Padding(2);
            this.btnTavukYemVer.Name = "btnTavukYemVer";
            this.btnTavukYemVer.Size = new System.Drawing.Size(174, 22);
            this.btnTavukYemVer.TabIndex = 9;
            this.btnTavukYemVer.Text = "YEM VER";
            this.btnTavukYemVer.UseVisualStyleBackColor = false;
            this.btnTavukYemVer.Click += new System.EventHandler(this.btnTavukYemVer_Click);
            // 
            // pbOrdek
            // 
            this.pbOrdek.Location = new System.Drawing.Point(113, 125);
            this.pbOrdek.Margin = new System.Windows.Forms.Padding(2);
            this.pbOrdek.Name = "pbOrdek";
            this.pbOrdek.Size = new System.Drawing.Size(174, 20);
            this.pbOrdek.TabIndex = 7;
            // 
            // label18
            // 
            this.label18.BackColor = System.Drawing.Color.Bisque;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label18.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label18.Location = new System.Drawing.Point(113, 103);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(174, 20);
            this.label18.TabIndex = 6;
            this.label18.Text = "CANLI";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pbTavuk
            // 
            this.pbTavuk.Location = new System.Drawing.Point(113, 54);
            this.pbTavuk.Margin = new System.Windows.Forms.Padding(2);
            this.pbTavuk.Name = "pbTavuk";
            this.pbTavuk.Size = new System.Drawing.Size(174, 20);
            this.pbTavuk.TabIndex = 4;
            // 
            // label15
            // 
            this.label15.BackColor = System.Drawing.Color.Bisque;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label15.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label15.Location = new System.Drawing.Point(113, 32);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(174, 20);
            this.label15.TabIndex = 3;
            this.label15.Text = "CANLI";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::B181210350.Properties.Resources.ordek;
            this.pictureBox2.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.InitialImage")));
            this.pictureBox2.Location = new System.Drawing.Point(2, 103);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(106, 68);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::B181210350.Properties.Resources.tavuk;
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(2, 32);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(106, 66);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // label23
            // 
            this.label23.BackColor = System.Drawing.Color.Orange;
            this.label23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label23.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label23.Location = new System.Drawing.Point(2, 7);
            this.label23.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(286, 21);
            this.label23.TabIndex = 0;
            this.label23.Text = "KÜMES";
            this.label23.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 366);
            this.Controls.Add(this.splitContainer1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lblKasa;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblSn;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnKeciHesapla;
        private System.Windows.Forms.Button btnInekHesapla;
        private System.Windows.Forms.Button btnOrdekHesapla;
        private System.Windows.Forms.Button btnTavukHesapla;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblKeciAdet;
        private System.Windows.Forms.Label lblOrdekAdet;
        private System.Windows.Forms.Label lblInekAdet;
        private System.Windows.Forms.Label lblTavukAdet;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btnKeciYemVer;
        private System.Windows.Forms.Button btnInekYemVer;
        private System.Windows.Forms.ProgressBar pbKeci;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ProgressBar pbInek;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnOrdekYemVer;
        private System.Windows.Forms.Button btnTavukYemVer;
        private System.Windows.Forms.ProgressBar pbOrdek;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ProgressBar pbTavuk;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Timer timer;
    }
}

